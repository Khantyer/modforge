package fr.nevah.minefield.blocks;

public class DoubleBasicSlab extends BasicSlab {

	public DoubleBasicSlab(String registryName) {
		super(registryName);
	}

	@Override
	public boolean isDouble() {
		return true;
	}

}
