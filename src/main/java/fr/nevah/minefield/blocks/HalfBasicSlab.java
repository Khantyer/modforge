package fr.nevah.minefield.blocks;

import net.minecraft.creativetab.CreativeTabs;

public class HalfBasicSlab extends BasicSlab {

	public HalfBasicSlab(String registryName) {
		super(registryName);
		this.setCreativeTab(CreativeTabs.tabBlock);
	}

	@Override
	public boolean isDouble() {
		return false;
	}

}
