package fr.nevah.minefield.blocks;

import net.minecraft.creativetab.CreativeTabs;

public class HalfLogSlab extends LogSlab {

	public HalfLogSlab(String registryName) {
		super(registryName);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	@Override
	public boolean isDouble() {
		return false;
	}

}
