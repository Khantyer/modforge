package fr.nevah.minefield.blocks;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import fr.nevah.minefield.MfBlock;
import fr.nevah.minefield.blocks.enums.BasicSlabType;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IStringSerializable;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class BasicSlab extends BlockSlab {

	public static final PropertyBool SEAMLESS = PropertyBool.create("seamless");
	public static final PropertyEnum VARIANT = PropertyEnum.create("variant", BasicSlabType.class);

	public BasicSlab(String registryName) {
		super(Material.rock);
		setRegistryName(registryName);
		setUnlocalizedName("basicSlab");
		IBlockState blockState = this.blockState.getBaseState();
		blockState = blockState.withProperty(VARIANT, BasicSlabType.SPRUCE);
		if (!this.isDouble()) {
			blockState = blockState.withProperty(HALF, EnumBlockHalf.BOTTOM);
		} else {
			blockState = blockState.withProperty(SEAMLESS, true);
		}
		setDefaultState(blockState);
	}
	
	@Override
	public String getUnlocalizedName(int meta) {
		return this.getUnlocalizedName() + "." + BasicSlabType.getStateFromMeta(meta).getName();
	}

	@Override
	public IProperty<?> getVariantProperty() {
		return VARIANT;
	}

	@Override
	public Object getVariant(ItemStack stack) {
		return BasicSlabType.getStateFromMeta(stack.getMetadata() & 7);
	}

	@Override
	public void getSubBlocks(Item itemIn, CreativeTabs tab, List<ItemStack> list) {
		Collection<BasicSlabType> collection = VARIANT.getAllowedValues();
		for (Iterator iterator = collection.iterator(); iterator.hasNext();) {
			BasicSlabType enumType = (BasicSlabType) iterator.next();
			switch (enumType) {
			case OAK:
			case SPRUCE:
			case BIRCH:
			case NETHER_BRICK:
				break;
			default:
				list.add(new ItemStack(this, 1, enumType.getMetadata()));
				break;
			}
			
		}
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta) {
		IBlockState blockState = this.getDefaultState();
		if (!this.isDouble()) {
			EnumBlockHalf value = EnumBlockHalf.BOTTOM;
			if ((meta & 8) != 0) {
				value = EnumBlockHalf.TOP;
			}

			blockState.withProperty(HALF, value);
		} else {
			blockState = blockState.withProperty(SEAMLESS, false);
		}
		blockState = blockState.withProperty(VARIANT, BasicSlabType.getStateFromMeta(meta & 7));

		return blockState;

	}

	@Override
	public final int getMetaFromState(final IBlockState state) {
		BasicSlabType enumType = (BasicSlabType) state.getValue(VARIANT);
		int meta = enumType.getMetadata();
		if (this.isDouble()) {
			return meta;
		}

		if ((EnumBlockHalf) state.getValue(HALF) == EnumBlockHalf.TOP) {
			return meta + 8;
		} else {
			return meta;
		}
	}

	@Override
	public final int damageDropped(final IBlockState state) {
		BasicSlabType enumType = (BasicSlabType) state.getValue(VARIANT);
		return enumType.getMetadata();
	}

	@Override
	protected final BlockState createBlockState() {
		if (this.isDouble()) {
			return new BlockState(this, new IProperty[] { VARIANT, SEAMLESS });
		} else {
			return new BlockState(this, new IProperty[] { VARIANT, HALF });
		}
	}

	@Override
	public final Item getItemDropped(final IBlockState blockState, final java.util.Random random, final int unused) {
		Item item = GameRegistry.findItem(MfBlock.MODID, "basic_slab");
		return item;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public final net.minecraft.item.Item getItem(final net.minecraft.world.World world,
			final net.minecraft.util.BlockPos blockPos) {
		Item item = GameRegistry.findItem(MfBlock.MODID, "basic_slab");
		return item;
	}

}
