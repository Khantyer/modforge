package fr.nevah.minefield.blocks.items;

import org.lwjgl.input.Mouse;

import fr.nevah.minefield.blocks.ClothStairs;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public class ItemStairs extends ItemBlock {

	public ItemStairs(Block block) {
		super(block);
		this.setMaxDamage(0);
		this.setHasSubtypes(true);
	}

	public String getUnlocalizedName(ItemStack stack) {
		ClothStairs clothStairs = (ClothStairs) this.block;
		return clothStairs.getUnlocalizedName(stack.getMetadata());
	}

	@Override
	public int getMetadata(int damage) {
		return damage;
	}
	
	@Override
	public boolean placeBlockAt(ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side,
			float hitX, float hitY, float hitZ, IBlockState newState) {
		Minecraft.getMinecraft().setIngameNotInFocus();
		return super.placeBlockAt(stack, player, world, pos, side, hitX, hitY, hitZ, newState);
	}
	
}
