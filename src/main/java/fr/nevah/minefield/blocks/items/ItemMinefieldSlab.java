package fr.nevah.minefield.blocks.items;

import fr.nevah.minefield.blocks.DoubleBasicSlab;
import fr.nevah.minefield.blocks.DoubleBrassSlab;
import fr.nevah.minefield.blocks.DoubleCopperSteelSlab;
import fr.nevah.minefield.blocks.DoubleLogSlab;
import fr.nevah.minefield.blocks.HalfBasicSlab;
import fr.nevah.minefield.blocks.HalfBrassSlab;
import fr.nevah.minefield.blocks.HalfCopperSteelSlab;
import fr.nevah.minefield.blocks.HalfLogSlab;
import net.minecraft.block.Block;
import net.minecraft.item.ItemSlab;

public class ItemMinefieldSlab extends ItemSlab {

	public ItemMinefieldSlab(Block block, HalfBasicSlab singleSlab, DoubleBasicSlab doubleSlab) {
		super(block, singleSlab, doubleSlab);
	}
	
	public ItemMinefieldSlab(Block block, HalfCopperSteelSlab singleSlab, DoubleCopperSteelSlab doubleSlab) {
		super(block, singleSlab, doubleSlab);
	}
	
	public ItemMinefieldSlab(Block block, HalfLogSlab singleSlab, DoubleLogSlab doubleSlab) {
		super(block, singleSlab, doubleSlab);
	}
	
	public ItemMinefieldSlab(Block block, HalfBrassSlab singleSlab, DoubleBrassSlab doubleSlab) {
		super(block, singleSlab, doubleSlab);
	}
}
