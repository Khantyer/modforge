package fr.nevah.minefield.blocks;

public class DoubleCopperSteelSlab extends CopperSteelSlab {

	public DoubleCopperSteelSlab(String registryName) {
		super(registryName);
	}

	@Override
	public boolean isDouble() {
		return true;
	}

}
