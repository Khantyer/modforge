package fr.nevah.minefield.blocks;

import net.minecraft.creativetab.CreativeTabs;

public class HalfBrassSlab extends BrassSlab {

	public HalfBrassSlab(String registryName) {
		super(registryName);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	@Override
	public boolean isDouble() {
		return false;
	}

}
