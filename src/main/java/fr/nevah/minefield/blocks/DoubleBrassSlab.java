package fr.nevah.minefield.blocks;

public class DoubleBrassSlab extends BrassSlab {

	public DoubleBrassSlab(String registryName) {
		super(registryName);
	}

	@Override
	public boolean isDouble() {
		return true;
	}

}
