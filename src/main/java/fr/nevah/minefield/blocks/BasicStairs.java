package fr.nevah.minefield.blocks;

import net.minecraft.block.BlockStairs;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.IStringSerializable;

public class BasicStairs extends BlockStairs {
		
	public BasicStairs(String name, IBlockState modelState) {
		super(modelState);
		setUnlocalizedName(name);
		setRegistryName(name);
		this.useNeighborBrightness = true;
	}

	public BasicStairs(String name, IBlockState modelState, CreativeTabs creativeTabs) {
		super(modelState);
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(creativeTabs);
		this.useNeighborBrightness = true;
	}
	
}
