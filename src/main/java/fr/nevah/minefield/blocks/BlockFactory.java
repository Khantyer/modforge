package fr.nevah.minefield.blocks;

import java.util.Collection;
import java.util.Iterator;

import fr.nevah.minefield.MfBlock;
import fr.nevah.minefield.blocks.items.ItemMinefieldSlab;
import fr.nevah.minefield.blocks.items.ItemStairs;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.util.IStringSerializable;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BlockFactory {

	public static void createBlock() {
		Block planks = GameRegistry.findBlock("minecraft", "planks");
		Block cobble = GameRegistry.findBlock("minecraft", "cobblestone");
		Block stone = GameRegistry.findBlock("minecraft", "stone");
		Block sandstone = GameRegistry.findBlock("minecraft", "sandstone");
		Block netherbrick = GameRegistry.findBlock("minecraft", "nether_brick");
		Block brick = GameRegistry.findBlock("minecraft", "brick_block");
		Block stonebrick = GameRegistry.findBlock("minecraft", "stonebrick");
		Block wool = GameRegistry.findBlock("minecraft", "wool");
		
		registerBlock(new BasicStairs("oak_planks_flat_stairs", planks.getStateFromMeta(0)));
		registerBlock(new BasicStairs("cobblestone_flat_stairs", cobble.getDefaultState()));

		Block paille = new BasicBlock("paille");
		registerBlock(paille);
		registerBlock(new BasicStairs("paille_stairs", paille.getDefaultState()));
		registerBlock(new BasicStairs("paille_flat_stairs", paille.getDefaultState()));

		Block ardoise = new BasicBlock("ardoise");
		registerBlock(ardoise);
		registerBlock(new BasicStairs("ardoise_stairs", ardoise.getDefaultState()));
		registerBlock(new BasicStairs("ardoise_flat_stairs", ardoise.getDefaultState()));

		registerSlab(new HalfBasicSlab("basic_slab"), new DoubleBasicSlab("double_basic_slab"), "*_slab");

		registerBlock(new BasicBlock("glowstone_pure").setLightLevel(1));
		
		createWoolStairs(wool);
		// TODO : Colored_glass
		registerBlock(new BasicStairs("stone_stairs", stone.getStateFromMeta(0)));
		registerBlock(new BasicStairs("sandstone_stairs", stone.getStateFromMeta(0)));

		// TODO : Marble
		// TODO : Jewel

		registerBlock(new BasicStairs("spruce_flat_stairs", planks.getStateFromMeta(1)));
		registerBlock(new BasicStairs("birch_flat_stairs", planks.getStateFromMeta(2)));
		registerBlock(new BasicStairs("jungle_flat_stairs", planks.getStateFromMeta(3)));
		registerBlock(new BasicStairs("stone_flat_stairs", stone.getStateFromMeta(0)));
		registerBlock(new BasicStairs("sandstone_flat_stairs", sandstone.getStateFromMeta(0)));
		registerBlock(new BasicStairs("netherbrick_flat_stairs", netherbrick.getDefaultState()));
		registerBlock(new BasicStairs("brick_flat_stairs", brick.getDefaultState()));
		registerBlock(new BasicStairs("stonebrick_flat_stairs", stonebrick.getStateFromMeta(0)));

		// TODO : copper
		// TODO : steel
		registerSlab(new HalfCopperSteelSlab("copper_steel_slab"),
				new DoubleCopperSteelSlab("double_copper_steel_slab"), "slab_*");
		// TODO : copper_steel_structure

		registerSlab(new HalfLogSlab("log_slab"), new DoubleLogSlab("double_log_slab"), "slab_log_*");
		
		registerSlab(new HalfBrassSlab("brass_slab"), new DoubleBrassSlab("double_brass_slab"), "slab_*");
	}

	private static void registerBlock(Block block) {
		GameRegistry.registerBlock(block, block.getRegistryName());
		MfBlock.proxy.registerBlockTexture(block);
	}

	private static void createWoolStairs(Block wool) {
		final String[][] colors = {{"white", "orange"}, {"magenta", "light_blue"}, {"yellow", "lime"}, {"pink", "gray"}, {"silver", "cyan"}, {"purple", "blue"}, {"brown", "green"}, {"red", "black"}}; 
		
		for (int i = 1; i < 9; i++) {
			ClothStairs clothStairs = new ClothStairs("cloth_stairs_" + i, wool.getDefaultState());
			GameRegistry.registerBlock(clothStairs, ItemStairs.class, clothStairs.getRegistryName());
			final String[] variants = {"minefield:" + colors[i-1][0] + "_cloth_stairs", "minefield:" + colors[i-1][1] + "_cloth_stairs"};
			MfBlock.proxy.registerBlockTextureWithVariant(clothStairs, variants);
		}
	}

	/***
	 * 
	 * @param halfSlab
	 * @param doubleSlab
	 * @param pattern
	 *            Une chaine de caractère représentant le nom du fichier.
	 *            Exemple : oak_slab.json -> pattern = *_slab
	 */
	private static void registerSlab(BlockSlab halfSlab, BlockSlab doubleSlab, String pattern) {

		GameRegistry.registerBlock(halfSlab, ItemMinefieldSlab.class, halfSlab.getRegistryName(), halfSlab, doubleSlab);
		GameRegistry.registerBlock(doubleSlab, null, doubleSlab.getRegistryName(), halfSlab, doubleSlab);

		Collection<IStringSerializable> collection = (Collection<IStringSerializable>) halfSlab.getVariantProperty()
				.getAllowedValues();

		String[] variants = new String[collection.size()];

		int i = 0;
		for (Iterator iterator = collection.iterator(); iterator.hasNext();) {
			IStringSerializable Type = (IStringSerializable) iterator.next();
			if (halfSlab instanceof BasicSlab)
				switch (Type.getName()) {
				case "spruce":
				case "nether_brick":
				case "birch":
				case "oak":
					variants[i] = pattern.replace("*", Type.getName());
					break;

				default:
					variants[i] = "minefield:" + pattern.replace("*", Type.getName());
					break;
				}
			else
				variants[i] = "minefield:" + pattern.replace("*", Type.getName());
			i++;
		}

		MfBlock.proxy.registerBlockTextureWithVariant(halfSlab, variants);
	}
}
