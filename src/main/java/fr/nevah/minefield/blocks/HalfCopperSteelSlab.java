package fr.nevah.minefield.blocks;

import net.minecraft.creativetab.CreativeTabs;

public class HalfCopperSteelSlab extends CopperSteelSlab {

	public HalfCopperSteelSlab(String registryName) {
		super(registryName);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	@Override
	public boolean isDouble() {
		return false;
	}

}
