package fr.nevah.minefield.blocks;

public class DoubleLogSlab extends LogSlab {

	public DoubleLogSlab(String registryName) {
		super(registryName);
	}

	@Override
	public boolean isDouble() {
		return true;
	}

}
