package fr.nevah.minefield.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BasicBlock extends Block {

	public BasicBlock(String name, CreativeTabs creativeTabs) {
		super(Material.rock);
		setHardness(3);
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(creativeTabs);
	}

	public BasicBlock(String name) {
		super(Material.rock);
		setHardness(3);
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(CreativeTabs.tabBlock);
	}
	
}
