package fr.nevah.minefield.blocks;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import fr.nevah.minefield.MfBlock;
import fr.nevah.minefield.blocks.enums.ClothStairsType;
import net.minecraft.block.BlockSlab.EnumBlockHalf;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ClothStairs extends BasicStairs {

	public static final PropertyEnum VARIANT = PropertyEnum.create("color", ClothStairsType.class);

	public ClothStairs(String name, IBlockState modelState) {
		super(name, modelState);
	}

	public String getUnlocalizedName(int metadata) {
		return this.getUnlocalizedName() + "." + ClothStairsType.getStateFromMeta(metadata).getName();
	}

	public IProperty<?> getVariantProperty() {
		return VARIANT;
	}

	public Object getVariant(ItemStack stack) {
		return ClothStairsType.getStateFromMeta(stack.getMetadata() & 8);
	}

	@Override
	public void getSubBlocks(Item itemIn, CreativeTabs tab, List<ItemStack> list) {
		Collection<ClothStairsType> collection = VARIANT.getAllowedValues();
		for (Iterator iterator = collection.iterator(); iterator.hasNext();) {
			ClothStairsType clothStairsType = (ClothStairsType) iterator.next();
			list.add(new ItemStack(this, 1, clothStairsType.getMetadata()));
		}
	}

	@Override
	public IBlockState getStateFromMeta(int meta) {
		Minecraft.getMinecraft().setIngameNotInFocus();
		IBlockState blockState = super.getStateFromMeta(meta);
		blockState = blockState.withProperty(VARIANT, ClothStairsType.getStateFromMeta(meta & 8));
		return blockState;
	}

	@Override
	public final int getMetaFromState(final IBlockState state) {
		ClothStairsType ClothStairsType = (ClothStairsType) state.getValue(VARIANT);
		return ClothStairsType.getMetadata();
	}

	@Override
	public final int damageDropped(final IBlockState state) {
		ClothStairsType ClothStairsType = (ClothStairsType) state.getValue(VARIANT);
		return ClothStairsType.getMetadata();
	}

	@Override
	protected final BlockState createBlockState() {
		return new BlockState(this, new IProperty[] { VARIANT, FACING, HALF, SHAPE });
	}

	@Override
	public final Item getItemDropped(final IBlockState blockState, final java.util.Random random, final int unused) {
		Item item = GameRegistry.findItem(MfBlock.MODID, this.getRegistryName());
		return item;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public final net.minecraft.item.Item getItem(final net.minecraft.world.World world,
			final net.minecraft.util.BlockPos blockPos) {
		Item item = GameRegistry.findItem(MfBlock.MODID, this.getRegistryName());
		return item;
	}

}
