package fr.nevah.minefield.blocks.enums;

import net.minecraft.util.IStringSerializable;

public enum ClothStairsType implements IStringSerializable {
	
	COLOR_1(0, "color_1"), COLOR_2(8, "color_2");
	
	private static final ClothStairsType[] METADATA = new ClothStairsType[2];
	private final String name;
	private final int metadata;

	private ClothStairsType(int metadata, String name) {
		this.metadata = metadata;
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	public int getMetadata() {
		return metadata;
	}

	@Override
	public String toString() {
		return name;
	}

	public static ClothStairsType getStateFromMeta(int metadata) {
		if (metadata == 8) {
			return METADATA[1];
		}

		return METADATA[0];
	}

	static {
		ClothStairsType[] var0 = values();
		int var1 = var0.length;

		for (int var2 = 0; var2 < var1; var2++) {
			ClothStairsType var3 = var0[var2];
			METADATA[var2] = var3;
		}
	}
}
