package fr.nevah.minefield.blocks.enums;

import net.minecraft.util.IStringSerializable;

public enum CopperSteelSlabType implements IStringSerializable {
	COPPER_BRICK(0, "copper_brick"), OXIDIZED_COPPER_BRICK(1, "oxidized_copper_brick"), VERY_OXIDIZED_COPPER_BRICK(
			2, "very_oxidized_copper_brick"), STEEL_BRICK(3, "steel_brick"), STEEL(4, "steel"), COPPER(5,
					"copper"), OXIDIZED_COPPER(6,
							"oxidized_copper"), VERY_OXIDIZED_COPPER(7, "very_oxidized_copper");

	private static final CopperSteelSlabType[] METADATA = new CopperSteelSlabType[values().length];
	private final String name;
	private final int metadata;

	private CopperSteelSlabType(int metadata, String name) {
		this.metadata = metadata;
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	public int getMetadata() {
		return metadata;
	}

	@Override
	public String toString() {
		return name;
	}

	public static CopperSteelSlabType getStateFromMeta(int metadata) {
		if (metadata < 0 || metadata >= METADATA.length) {
			metadata = 0;
		}

		return METADATA[metadata];
	}

	static {
		CopperSteelSlabType[] var0 = values();
		int var1 = var0.length;

		for (int var2 = 0; var2 < var1; var2++) {
			CopperSteelSlabType var3 = var0[var2];
			METADATA[var3.getMetadata()] = var3;
		}
	}
}
