package fr.nevah.minefield.blocks.enums;

import net.minecraft.util.IStringSerializable;

public enum BasicSlabType implements IStringSerializable {
	OAK(0, "oak"), SPRUCE(1, "spruce"), BIRCH(2, "birch"), PAILLE(3, "paille"), ARDOISE(4, "ardoise"), NETHER_BRICK(
			5, "nether_brick"), MARBRE_BLANC(6, "marbre_blanc"), MARBRE_NOIR(7, "marbre_noir");

	private static final BasicSlabType[] METADATA = new BasicSlabType[values().length];
	private final String name;
	private final int metadata;

	private BasicSlabType(int metadata, String name) {
		this.metadata = metadata;
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	public int getMetadata() {
		return metadata;
	}

	@Override
	public String toString() {
		return name;
	}

	public static BasicSlabType getStateFromMeta(int metadata) {
		if (metadata < 0 || metadata >= METADATA.length) {
			metadata = 0;
		}

		return METADATA[metadata];
	}

	static {
		BasicSlabType[] var0 = values();
		int var1 = var0.length;

		for (int var2 = 0; var2 < var1; var2++) {
			BasicSlabType var3 = var0[var2];
			METADATA[var3.getMetadata()] = var3;
		}
	}
}
