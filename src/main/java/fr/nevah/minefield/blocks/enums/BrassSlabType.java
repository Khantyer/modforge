package fr.nevah.minefield.blocks.enums;

import net.minecraft.util.IStringSerializable;

public enum BrassSlabType implements IStringSerializable {
	BRASS_BRICK(0, "brass_brick"), OXIDIZED_BRASS_BRICK(1, "oxidized_brass_brick"), VERY_OXIDIZED_BRASS_BRICK(2,
			"very_oxidized_brass_brick"), BRASS(3,
					"brass"), OXIDIZED_BRASS(4, "oxidized_brass"), VERY_OXIDIZED_BRASS(5, "very_oxidized_brass");

	private static final BrassSlabType[] METADATA = new BrassSlabType[values().length];
	private final String name;
	private final int metadata;

	private BrassSlabType(int metadata, String name) {
		this.metadata = metadata;
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	public int getMetadata() {
		return metadata;
	}

	@Override
	public String toString() {
		return name;
	}

	public static BrassSlabType getStateFromMeta(int metadata) {
		if (metadata < 0 || metadata >= METADATA.length) {
			metadata = 0;
		}

		return METADATA[metadata];
	}

	static {
		BrassSlabType[] var0 = values();
		int var1 = var0.length;

		for (int var2 = 0; var2 < var1; var2++) {
			BrassSlabType var3 = var0[var2];
			METADATA[var3.getMetadata()] = var3;
		}
	}
}
