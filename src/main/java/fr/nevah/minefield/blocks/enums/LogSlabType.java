package fr.nevah.minefield.blocks.enums;

import net.minecraft.util.IStringSerializable;

public enum LogSlabType implements IStringSerializable {
	OAK(0, "oak"), SPRUCE(1, "spruce"), BIRCH(2, "birch"), JUNGLE(3, "jungle"), ACACIA(4, "acacia"), DARK_OAK(5,
			"dark_oak");

	private static final LogSlabType[] METADATA = new LogSlabType[values().length];
	private final String name;
	private final int metadata;

	private LogSlabType(int metadata, String name) {
		this.metadata = metadata;
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	public int getMetadata() {
		return metadata;
	}

	@Override
	public String toString() {
		return name;
	}

	public static LogSlabType getStateFromMeta(int metadata) {
		if (metadata < 0 || metadata >= METADATA.length) {
			metadata = 0;
		}

		return METADATA[metadata];
	}

	static {
		LogSlabType[] var0 = values();
		int var1 = var0.length;

		for (int var2 = 0; var2 < var1; var2++) {
			LogSlabType var3 = var0[var2];
			METADATA[var3.getMetadata()] = var3;
		}
	}
}