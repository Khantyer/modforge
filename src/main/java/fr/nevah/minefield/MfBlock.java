package fr.nevah.minefield;

import fr.nevah.minefield.blocks.BlockFactory;
import fr.nevah.minefield.proxy.CommonProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

@Mod(modid = MfBlock.MODID, version = MfBlock.VERSION)
public class MfBlock
{
    public static final String MODID = "minefield";
    public static final String VERSION = "2.0";
    
    @SidedProxy(clientSide = "fr.nevah.minefield.proxy.ClientProxy", serverSide = "fr.nevah.minefield.proxy.CommonProxy")
    public static CommonProxy proxy;
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
		BlockFactory.createBlock();
    }
}
