package fr.nevah.minefield.proxy;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemModelMesher;
import net.minecraft.client.resources.model.ModelBakery;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public class ClientProxy extends CommonProxy {

	@Override
	public void registerItemTexture(Item item, int metadata, String registryName) {
		ItemModelMesher mesher = Minecraft.getMinecraft().getRenderItem().getItemModelMesher();
		mesher.register(item, metadata, new ModelResourceLocation(registryName, "inventory"));
	}

	@Override
	public void registerBlockTexture(Block block, int metadata, String registryName) {
		registerItemTexture(Item.getItemFromBlock(block), metadata, registryName);
	}
	
	@Override
	public void registerBlockTexture(Block block, int metadata) {
		registerItemTexture(Item.getItemFromBlock(block), metadata, block.getRegistryName());
	}

	@Override
	public void registerBlockTexture(Block block) {
		registerBlockTexture(block, 0);
	}
	
	@Override
	public void registerBlockTextureWithVariant(Block block, String[] variant) {
		ResourceLocation[] resourceLocations = new ResourceLocation[variant.length];
		for (int i = 0; i < variant.length; i++) {
			resourceLocations[i] = new ResourceLocation(variant[i]);
		}
		
		ModelBakery.registerItemVariants(Item.getItemFromBlock(block), resourceLocations);
		
		for (int i = 0; i < resourceLocations.length; i++) {
			this.registerBlockTexture(block, i, variant[i]);
		}
	}
}
