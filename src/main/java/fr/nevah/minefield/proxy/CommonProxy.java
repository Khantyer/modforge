package fr.nevah.minefield.proxy;

import net.minecraft.block.Block;
import net.minecraft.item.Item;

public class CommonProxy {

	public void registerItemTexture(Item item, int metadata, String name) {}

	public void registerBlockTexture(Block block, int metadata) {}

	public void registerBlockTexture(Block block) {}
	
	public void registerBlockTexture(Block block, int metadata, String registryName) {}

	public void registerBlockTextureWithVariant(Block block, String[] variantName) {}

}
