## Bienvenue sur le dépot du mod MfBlock ! ##

### Première utilisation ###

Vous pouvez mettre ce dépot en français depuis la configuration de votre profil. Pour pouvoir commencer à coder vous devrez tout d'abord installer [Git](http://git-scm.com/). Une fois que git est installé lancez l'invite de commande qui permet d'utiliser git et executez ces commandes :

* git config --global user.name "votre_pseudo" (Mettez le pseudo de votre compte ici)
* git config --global user.email moi@email.com (Encore une fois mettez votre adresse email)

Ces 2 commandes ont permis de configurer Git pour que vous puissiez récupérer les sources ici et mettre à jour. Maintenant, depuis la console déplacez vous grace à la commande cd (sensible à la casse et tab complète les noms) allez dans mes documents par exemple et copiez collez la commande contenue dans l'onglet "Cloner" et collez là dans l'invite de commande. Ceci vous créera un dossier "mfblock" dans lequel sera contenu les sources. 

Maintenant, telechargez les [sources de forge](http://files.minecraftforge.net/maven/net/minecraftforge/forge/1.7.10-10.13.0.1180/forge-1.7.10-10.13.0.1180-src.zip), puis décompressez le fichier dans le dossier "mfblock". Ouvrez alors un terminal et tapez gradlew setupDecompWorkspace eclipse. Après l'installation ouvrez eclipse et utilisez le dossier "eclipse" contenu dans le dossier "mfblock" comme Workspace.

### Contribuer ###

Pour proposer des modifications, cliquez sur l'onglet "Créer la branche", dans la liste sélectionner la branche master et choisissez un nom pour votre branche. Copiez collez dans le terminal de Git la commande qui vous est donnée (git fetch && git checkout nomdevotrebranche).

Une fois que vous aurez fini d'éditer vous pouvez créer un commit en local. Ouvrez le terminal git et tapez la commande git commit -a vous pourrez alors commenter ce que vous avez fait. Vous devrez alors les "pousser" vers le serveur par la commande git push et créer votre demande d'ajout.

### Convention de contribution ###

Vos contribution doivent être formaté avec ce [formatter](http://www.khantyer.fr/MfBlock/Contrib/MfBlock.xml) quand elles seront proposé, bien sûr le reste du temps vous faites comme vous voulez :smirk:. Pour importer un formatter Window -> Preferences -> Java -> Code Style -> Formatter. Le raccourcis pour formater est Ctrl+Shift+F ou clic droit source -> Format. Vous devrez aussi "signer" la classe que vous avez créez comme ceci :

```
#!java

/***
*@author Khantyer
*@version 1.0
*@date 17/08/2014
***/
```
Si vous la modifiez et que vous n'êtes pas l'auteur d'origine :

```
#!java

/***
*@author Khantyer
*@contributor Miti16
*@version 1.1
*@date 17/08/2014
***/
```